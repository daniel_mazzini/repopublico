﻿using Xunit;

namespace Libreria.Xunit.Test
{
    public class CalculadoraTest
    {
        [Fact]
        public void DosMasDosSonCuatro()
        {
            var sut = new Calculadora();
            var result = sut.Sumar(2, 2);

            Assert.Equal(4, result);
        }
    }
}
